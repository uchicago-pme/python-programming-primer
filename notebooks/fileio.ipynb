{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Interactive Input &  FILE I/O"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Up to now all our programs have run in their little bubble: they define the variable values they use and after they finish they leave little trace that they were ever run. \n",
    "\n",
    "Dealing and interacting with files is a very common way of processing information stored in a computer, and is something that your programs might often do. When we deal with files and our programs go beyond their \"logical bubble\", is when we can start doing some damage: delete information on files, delete files all together, fill up the hard disk, damage your installation (the operating system should actually have safeguards for this). This should not intimidate you, it is just that with power it comes responsibility.\n",
    "\n",
    "In this notevook we will first go over the basic operations on files. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interactivity: reading user input"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One important aspect when running a program outside of the notebook, where the user cannot see or change variable values easily, is to provide a way to prompt the user for input. The function *input()* prompts the user and collects input typed on the keyboard. This is the simplest way to create interactive programs.\n",
    "\n",
    "    keys=input(\"I will collect what you type: \")\n",
    "    print(\"What you typed was:\",keys)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first line of code will present the user with a string, and in the notebook a text box where whatever you type on the keyboard until you hit return is \"collected\" (outside of the notebook this textbox does not appear, but we will have the chance to check the behaviour later on). Whatever you type is stored as a string in variable *keys* in this case.\n",
    "\n",
    "If we expect numerical input from the user, the string needs to be converted.\n",
    "\n",
    "    number_string=input(\"How many carbon atoms are there in butene?\")\n",
    "    number_number=int(number_string)\n",
    "    if number_number == 4:\n",
    "       print(\"Obviously!\")\n",
    "    else:\n",
    "       print(\"Are you a physicist, or what?!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dealing with files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In any computer hard-disk there are typically two types of files: text files and binary files. Text files are those you can read with a text editor program (think of Notepad, not Microsoft Word), and do not necessarily contain prose. (If you open this notebook, which has an extension \\*.ipynb, on a text editor you will see that it is text in some form. The Jupyter notebook is a text file.) Binary files on the other end cannot be opened on a text editor, or when they do open they show up as ununderstandable characters. (Picture files like PNG or JPEG, music files like MP3, video files, Word DOC files, are all binary files.)\n",
    "\n",
    "We will only be looking at manipulating text files. Although Python can also manipulate binary files, the caveat of such files is that the programmer needs to know *a priori* how the content of the file is structured, and since one cannot just look at the file on a text editor, this task is harder. Binary files further require knowledge of some low level computer architecture which is beyond the scope of this course. Manipulating text files will however illustrate the process, and most instruments and computer programs are able to write data in some form of text file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will start by loading the content of the file <a href=\"test.out\">test.out</a> (you can open the file to look at its content). First we need to open a stream to the file content and set it to a variable. This is done with function *open()*\n",
    "\n",
    "    stream1=open('test.out','r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This takes a string containing the name of our file (including the full path if the file is not in the same directory as the notebook/script) and as a second argument a flag telling Python whether we are going to read or write to the file. ('r' for reading and 'w' for writing).\n",
    "\n",
    "Via the variable *stream1* we now have direct access to the content of the file. If we loop over the file stream we can read the lines of the file one by one as strings\n",
    "\n",
    "    for line in stream1:\n",
    "        print(line)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we are done with the file we should tidy up, close the stream and leave the file in peace\n",
    "\n",
    "    stream1.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we are interested in a list with every line in the file we could build a list with a loop, or simply operate with the function *list()* on the stream\n",
    "\n",
    "    stream2=open('test.out','r')\n",
    "    list_content=list(stream2)\n",
    "    stream2.close()\n",
    "    list_content"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note at the end of each line/list element the linebreak represented by '\\n' which counts as a single character.\n",
    "\n",
    "We should be slightly careful when performing this operations as it puts all the content of the file into a list, which will be unmanageable if the file is several gigabytes in size.\n",
    "\n",
    "It is just as simple to read a specific number of lines, using a loop and the method *.readline()*\n",
    "\n",
    "    stream3=open('test.out','r')\n",
    "    for i in range(2):\n",
    "        line=stream3.readline()\n",
    "        print(line)\n",
    "    stream3.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The house keeping step of closing the stream can be a bit tedious an easily forgotten. The following construct will close the stream for us when we are done\n",
    "\n",
    "    with open('test.out','r') as stream:\n",
    "        list_content_again=list(stream)\n",
    "    list_content_again"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we are just interested in some specific information in the file we can search for it as we read it.\n",
    "\n",
    "The following code looks for the line with the substring 'leaves' and extracts the colour of the leaves in the text."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('test.out','r') as stream:\n",
    "    for line in stream:\n",
    "        if 'leaves' in line:\n",
    "            leaf_colour = line.split()[4]\n",
    "        \n",
    "leaf_colour[:-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the variable *line* is a string with each line in the file. If we are interested in individual words, we can form a list from a string with the **.split()** method.\n",
    "\n",
    "    \"Read my words. One by one.\".split()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default *.split()* separates the string on the blank spaces (space or tab characters), but we can choose any other character\n",
    "\n",
    "    \"Read my words. One by one.\".split(\".\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For completeness, the opposite operation to *.split()* is performed by *.join()*\n",
    "\n",
    "    \"--\".join(['three','two','one','go'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is it clear how we are obtaining the colour of the leaves? Write some code below that extracts the colour of the sky instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The authors of those words were probably not in their best mood. So we are going to change the text to make it more cheerful (even if slightly psychedelic). The goal is thus to construct a list of the verses in the lyrics, but with *blue* leaves, a *bay* sky and a *glorious* day. Let us call this list *cheerful*.\n",
    "\n",
    "This task could be done in one go with a single *for* loop (if you are feeling confortable you can try to implement such solution). We will however break the task into two. First, create the list *cheerful* where each element is a verse of the original text split into words."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now change your list such that you replace the wanted words in the text, and put each verse together using the *.join()* mehtod such that *cheerful* is a list of strings with the verses of the lyrics."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we want to write our cheerful version to a file, we just do\n",
    "\n",
    "    with open('cheerful.out','w') as stream:\n",
    "        stream.writelines(cheerful)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that if you open a file for writing you **will overwrite whatever was initially in the file**.\n",
    "\n",
    "The <a href\"https://docs.python.org/3/library/os.html\">os module</a> provides many functions for interaction with the operating system, including the file system. We can get the list of the files in the current directory and see if our new created file is in place\n",
    "\n",
    "    import os\n",
    "    os.listdir()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Files are an important way of permanent storage of data. Handling files is important not only for processing data generated by instruments or other programs, but also to store results generated by your own programs.\n",
    "\n",
    "In this notebook we have seen how to access data in files. The function *open()* creates a stream to access the file content, which can be looped line by line. Each line is retrieved as a string, in which context the string method *.split()* becomes useful for further processing.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
